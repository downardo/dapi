package main.java.de.downardo.dapi;

public class AsciiApi {
	
	public static int getASCIIfromLetter(char letter){
		int asciiCode = 0;
		asciiCode = Integer.valueOf(letter);
		return asciiCode;
	}
	
	public static String getLetterfromASCII(int asciiCode){
		String letter = null;
		letter = String.valueOf(Character.toChars(asciiCode));
		return letter;
	}

}
